function notification()
{
    var first_count = 0;
    var second_count = 0;
    var postFirst_count = 0;
    var postSecond_count = 0;

    if(Notification.permission !== "granted")
        Notification.requestPermission();
    else{
        var ref = firebase.database().ref('com_list');
        ref.once('value').then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var key = childSnapshot.key;
                var data = childSnapshot.val();
                var commentRef = firebase.database().ref('com_list/' + key + '/comment');
                commentRef.once('value').then(function(commentSnapshot){
                    commentSnapshot.forEach(function(commentChildSnapshot){
                        first_count += 1;
                    });
                    commentRef.on('child_added', function(addedData){
                        second_count += 1;
                        if(second_count > first_count)
                        {
                            var user = firebase.auth().currentUser;
                            var childData = addedData.val();
                            var writer = childData.writer;
                            var uid = childData.writerUid;
                            if((user.uid == data.writerUid) && (data.writerUid != uid))
                            {
                                var notification = new Notification('New comment!', {
                                    icon: 'https://firebasestorage.googleapis.com/v0/b/small-forum-d5537.appspot.com/o/head_shot%2Fphoto.png?alt=media&token=be15658b-7f48-4ab6-b29a-4b4c8ad8e2a0',
                                    body: writer + " has commented your post!",
                                });
                                notification.onclick = function(){
                                    window.open("article.html?" + key);
                                    notification.close();
                                };
                            }
                        }
                    });
                });
            });
            snapshot.forEach(function(childSnapshot){
                postFirst_count += 1;
            });
            ref.on('child_added', function(childSnapshot){
                postSecond_count += 1;
                if(postSecond_count > postFirst_count)
                {
                    var key = childSnapshot.key;
                    var data = childSnapshot.val();
                    var commentRef = firebase.database().ref('com_list/' + key + '/comment');
                    commentRef.once('value').then(function(commentSnapshot){
                        commentSnapshot.forEach(function(commentChildSnapshot){
                            first_count += 1;
                        });
                        commentRef.on('child_added', function(addedData){
                            second_count += 1;
                            if(second_count > first_count)
                            {
                                var user = firebase.auth().currentUser;
                                var childData = addedData.val();
                                var writer = childData.writer;
                                if((user.uid == data.writerUid) && (data.writerUid != uid))
                                {
                                    var notification = new Notification('New Comment!', {
                                        icon: 'https://firebasestorage.googleapis.com/v0/b/small-forum-d5537.appspot.com/o/head_shot%2Fphoto.png?alt=media&token=be15658b-7f48-4ab6-b29a-4b4c8ad8e2a0',
                                        body: writer + " has commented your post!",
                                    });
                                    notification.onclick = function(){
                                        window.href("article.html?" + key);
                                        notification.close();
                                    };
                                }
                            }
                        });
                    });
                }
            });
        });
    }
}