function init() 
{
    notification();
    var currentPage = parseInt(location.href.split('?')[1]) || 1;
    var pages = document.getElementById('pages');
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('menu');
        if (user) 
        {
            if(!user.emailVerified)
                document.getElementById('newPost').style.display = "none";
            document.getElementById('newPost').style.visibility = 'visible';
            var email = user.email;
            var displayName = user.displayName;
            var photoURL = user.photoURL;
            var profileURL = "profile.html?" + user.uid;
            menu.innerHTML = "<li class='nav-item dropdown'>" +
            "<a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" +
            "<img class = 'rounded' src = " + photoURL + " alt = '' height = '30' width = '30'> " + displayName + "</a><div class='dropdown-menu' aria-labelledby='navbarDropdown'><a class='dropdown-item' href=" + profileURL + ">Profile</a><div class='dropdown-divider'></div>" +
            "<a class='dropdown-item' href='historyPost.html'>History Post</a></div></li><li class='nav-item'><a class='nav-link' id = 'btnLogout' href='#'>Log out</a></li>";
            var btnLogout = document.getElementById('btnLogout');
            btnLogout.addEventListener('click', function(){
                firebase.auth().signOut().then(function(){
                    alert("Log out Success!");
                }).catch(function(e){
                    alert(e.message);
                });
            });
            if(currentPage == 1)
            {
                pages.innerHTML = "<input type = 'button' id = 'firstPage' value = '1' class = 'box-shadow rounded' style = 'margin: 20px 5px; background-color: rgba(0,0,0,0.2); border: 0;'><input type = 'button' id = 'secondPage' value = '2' style = 'margin: 20px 5px; border: 0; background-color: transparent;'><input type = 'button' id = 'thirdPage' value = '3' style = 'margin: 20px 5px; border: 0; background-color: transparent;'><input type = 'button' id = 'next' value = 'Next' style = 'border-radius: 5px; background-color: gray; color: white; margin: 20px 5px;'>";
                document.getElementById('secondPage').addEventListener('click', function(){
                    window.location.href = "index.html?2";
                }, false);
                document.getElementById('thirdPage').addEventListener('click', function(){
                    window.location.href = "index.html?3";
                }, false);
                document.getElementById('next').addEventListener('click', function(){
                    window.location.href = "index.html?2";
                }, false);
            }
            else
            {
                pages.innerHTML = "<input type = 'button' id = 'prev' value = 'Prev' style = 'border-radius: 5px; background-color: gray; color: white; margin: 20px 5px;'><input type = 'button' id = 'firstPage' value = " + (currentPage - 1) + " style = 'margin: 20px 5px; border: 0; background-color: transparent;'><input type = 'button' id = 'secondPage' value = " + currentPage + " class = 'rounded box-shadow' style = 'margin: 20px 5px; border: 0; background-color: rgba(0,0,0,0.2);'><input type = 'button' id = 'thirdPage' value = " + (currentPage + 1) + " style = 'margin: 20px 5px; border: 0; background-color: transparent;'><input type = 'button' id = 'next' value = 'Next' style = 'border-radius: 5px; background-color: gray; color: white; style = 'margin: 20px 5px;''>";
                document.getElementById('prev').addEventListener('click', function(){
                    window.location.href = "index.html?" + (currentPage - 1);
                }, false);
                document.getElementById('firstPage').addEventListener('click', function(){
                    window.location.href = "index.html?" + (currentPage - 1);
                }, false);
                document.getElementById('thirdPage').addEventListener('click', function(){
                    window.location.href = "index.html?" + (currentPage + 1);
                }, false);
                document.getElementById('next').addEventListener('click', function(){
                    window.location.href = "index.html?" + (currentPage + 1);
                }, false);
            }
        } 
        else 
        {
            menu.innerHTML = "<li class='nav-item'><a class='nav-link' href='login.html'>Log in</a></li><li class='nav-item'><a class='nav-link' href='signup.html'>Sign up</a></li>"
            document.getElementById('post_list').innerHTML = "";
            pages.innerHTML = "";
            document.getElementById('newPost').style.display = 'none';
            document.getElementById('loading').style.display = "none";
        }
    });

    var str_before_title = "<div class = 'my-1 p-3 rounded box-shadow text-muted media'>";
    var str_after_title = "</div>\n";

    var pageNum = (currentPage-1)*10+1;
    var postsRef = firebase.database().ref('com_list').orderByChild('sortTime').limitToFirst(pageNum);
    var total_post = [];
    var pageCount = 0;
    var theFirstPostTime = 0;
    var first_count = 0;
    var second_count = 0;
    var tmp = 0;
    var tmpTitle = [];

    postsRef.once('value').then(function(snapshot){
        snapshot.forEach(function(childSnapshot){
            pageCount += 1;
            if(pageCount == pageNum)
            {
                theFirstPostTime = childSnapshot.val().sortTime;
            }
        });
        var pageRef = firebase.database().ref('com_list').orderByChild('sortTime').startAt(theFirstPostTime).limitToFirst(12);
        pageRef.once('value').then(function(childSnapshot){
            childSnapshot.forEach(function (grandSnapshot){
                var childData = grandSnapshot.val();
                var articleTitle = childData.title;
                var articleUpdateTime = childData.updateTime;
                var dataLink = "article.html?" + grandSnapshot.key;
                var tmp2 = tmp.toString();
                total_post[total_post.length] = str_before_title + "<a class = 'media-body' href = " + dataLink + "><span id = " + tmp2 + "></span>" + "</a><span>" + articleUpdateTime + "</span>" + str_after_title;
                tmpTitle[tmpTitle.length] = articleTitle;
                tmp += 1;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');
            for(i = 0; i < tmp; i++)
            {
                var tmp2 = i.toString();
                document.getElementById(tmp2).innerText = tmpTitle[i];
            }
        });
    }).then(function(){
            document.getElementById('loading').style.display = "none";
    }).catch(e => console.log(e.message));
}

window.onload = function()
{
    init();
};
