function init()
{
    var txtUserName = document.getElementById('inputUserName');
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnFacebook = document.getElementById('btnFacebook');
    var btnGoogle = document.getElementById('btnGoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnGoogle.addEventListener('click', function(){
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            var token = result.credential.accessToken;
            var user = result.user;
            var uid = result.uid;
            var ref = firebase.database().ref('users/' + user.uid);
            ref.once('value').then(function(snapshot){
                if(!snapshot.exists())
                {
                    var data = {
                        photoURL: user.photoURL,
                        postTimes: 0,
                        commentTimes: 0
                    }
                    ref.set(data);
                }
                setTimeout(function(){
                    window.location.href = "index.html?1";
                },1000);
            });
        }).catch(function(error){
            var errorMessage = error.message;
            create_alert(errorMessage); 
        });
    });

    btnFacebook.addEventListener('click', function(e){
        var provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            var uid = result.uid;
            var ref = firebase.database().ref('users/' + user.uid);
            ref.once('value').then(function(snapshot){
                if(!snapshot.exists())
                {
                    var data = {
                        photoURL: user.photoURL,
                        postTimes: 0,
                        commentTimes: 0
                    }
                    ref.set(data);
                }
                setTimeout(function(){
                    window.location.href = "index.html?1";
                },1000);
            });
        }).catch(function(error){
            var errorMessage = error.message;
            create_alert(errorMessage);
        });
    });

    btnSignUp.addEventListener('click', function(){        
        var email = txtEmail.value;
        var password = txtPassword.value;
        var userName = txtUserName.value;
        var defaultHeadShot = '';

        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(currentUser){
            currentUser.updateProfile({
                displayName: userName,
                photoURL: "https://firebasestorage.googleapis.com/v0/b/small-forum-d5537.appspot.com/o/head_shot%2Fman.png?alt=media&token=7756ec7d-46fd-470d-bd90-c372bf3b24e9"
            });
            var data = {
                photoURL: "https://firebasestorage.googleapis.com/v0/b/small-forum-d5537.appspot.com/o/head_shot%2Fman.png?alt=media&token=7756ec7d-46fd-470d-bd90-c372bf3b24e9",
                postTimes: 0,
                commentTimes: 0
            }
            var userPath = firebase.database().ref("users/" + currentUser.uid);
                userPath.set(data);
            setTimeout(function(){
                txtEmail.value = "";
                txtPassword.value = "";
                window.location.href = "index.html?1";
            },1000);
        }).catch(function(error){
            txtEmail.value = "";
            txtPassword.value = "";
            var errorMessage = error.message;
            create_alert(errorMessage);
        });
    });
}

function create_alert(message) 
{
    var alertarea = document.getElementById('custom-alert');
    alertarea.innerHTML = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";;
}

window.onload = function () 
{
    init();
};