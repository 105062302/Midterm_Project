window.onload = init;

function splitTime(str)
{
    var day = str.split(' ')[1];
    var month = str.split(' ')[2];
    var year = str.split(' ')[3];
    var time = str.split(' ')[4];
    var splitTime = year + "-" + month + "-" + day + " " + time;
    return splitTime;
}

function init()
{
    var url = location.href;
    var key = "/" + url.split('?')[1];
    var link = '';
    var emailVerified = '';
    var creationTime = '';
    var lastLogInTime = '';
    var postTimes = '';
    var commentTimes = '';

    notification();

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) 
        {
            if(user.uid != key.split('/')[1])
            {
                window.location.href = "index.html?1";
            }
            var menu = document.getElementById('menu');
            var profileURL = "profile.html?" + user.uid;
            menu.innerHTML = "<li class='nav-item dropdown'>" +
            "<a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" +
            "<img class = 'rounded' src = " + user.photoURL + " alt = '' height = '30' width = '30'> " + user.displayName + "</a><div class='dropdown-menu' aria-labelledby='navbarDropdown'><a class='dropdown-item' href=" + profileURL + ">Profile</a><div class='dropdown-divider'></div>" +
              "<a class='dropdown-item' href=" + "historyPost.html?" + user.uid + ">History Post</a></div></li><li class='nav-item'><a class='nav-link' href='index.html?1'>Home</a></li>";
            link = user.photoURL;
            creationTime = splitTime(user.metadata.creationTime);
            lastLogInTime = splitTime(user.metadata.lastSignInTime);
            var ref = firebase.database().ref("users/" + user.uid);
            ref.once('value').then(function (snapshot) {
                var data = snapshot.val();
                postTimes = data.postTimes;
                commentTimes = data.commentTimes;
                if(user.emailVerified)
                {
                    emailVerified = "Yes";
                    document.getElementById('btnVerified').style.display = "none";
                    document.getElementById('verifiedNotice').style.display = "none";
                }
                else
                    emailVerified = "No";
                document.getElementById("headShotPhoto").src = link;
                document.getElementById("displayName").innerHTML = user.displayName;
                document.getElementById("s_displayName").innerHTML = user.displayName + "(UID: " + user.uid + ")";
                document.getElementById("userEmail").innerHTML = "Email: " + user.email;
                document.getElementById("verified").innerHTML = "Email Verified: " + emailVerified;
                document.getElementById("creationTime").innerHTML = "Account Creation Time: " + creationTime;
                document.getElementById("loginTime").innerHTML = "Last Log In Time: " + lastLogInTime;
                document.getElementById("postTimes").innerHTML = "Post Times: " + postTimes;
                document.getElementById("commentTimes").innerHTML = "Comment Times: " + commentTimes;
            }).then(function(){
                document.getElementById('loading').style.display = "none";
            });
            var uploadPic = document.getElementById("picture");
            uploadPic.addEventListener('change', function(){
                var file = this.files[0];
                var ref = firebase.storage().ref('head_shot/' + user.uid + '/' + user.uid).put(file);            
                ref.on('state_changed', function(){}, function(error){}, function(){
                        var newPhotoURL = firebase.storage().ref('head_shot/' + user.uid + '/' + user.uid);
                        newPhotoURL.getDownloadURL().then(function(url){
                            user.updateProfile({
                                photoURL: url
                            });
                            firebase.database().ref('users/' + user.uid).update({
                                photoURL: url
                            });
                            setTimeout(function(){
                                location.reload();
                            },1000);
                        });
                    });
            },false);
            document.getElementById('btnVerified').addEventListener('click', function(){
                var user = firebase.auth().currentUser;
                user.sendEmailVerification().then(function() {
                    alert("Confirmation email was sent !")
                  }, function(error) {
                    console.error("Fail to send Confirmation email!");
                  });
            }, false);
        } 
    });
}


