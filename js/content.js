function init()
{
    var btnPost = document.getElementById('btnPost');
    var content = document.getElementById('content');
    var title = document.getElementById('title');
    notification();
    firebase.auth().onAuthStateChanged(function(user){
        if(user)
        {
            if(!user.emailVerified)
                window.location.href = "index.html?1";
        }
        else 
        {
            window.location.href = "index.html?1";
        }
    });
    btnPost.addEventListener('click', function(){
        var currentUser = firebase.auth().currentUser;
        if (title.value != "" && content.value != "") {
            var ref = firebase.database().ref('com_list');
            var nowTime = new Date();
            var year = nowTime.getFullYear();
            var month = (nowTime.getMonth()+1 < 10 ? '0' : '') + (nowTime.getMonth()+1);
            var date = (nowTime.getDate() < 10 ? '0' : '') + (nowTime.getDate());
            var hour = (nowTime.getHours() < 10 ? '0' : '') + (nowTime.getHours());
            var minute = (nowTime.getMinutes() < 10 ? '0' : '') + (nowTime.getMinutes());
            var second = (nowTime.getSeconds() < 10 ? '0' : '') + (nowTime.getSeconds());
            var postTime = year + "-" + month + "-" + date + " " + hour + ":" + minute;
            var data = {
                title: title.value,
                writer: currentUser.displayName,
                writerUid: currentUser.uid,
                content: content.value,
                postTime: postTime,
                updateTime: postTime,
                sortTime: parseInt('-' + year + month + date + hour + minute + second)
            };
            ref.push(data);
            var postRef = firebase.database().ref('users/' + currentUser.uid);
            postRef.once('value').then(function(snapshot){
                var data = snapshot.val();
                var postTimes = parseInt(data.postTimes) + 1;
                firebase.database().ref('users/' + currentUser.uid).update({
                    postTimes: postTimes
                });
                setTimeout(function(){
                    title.value = "";
                    content.value = "";
                    window.location.href = "index.html?1";
                },1000);
            });
        }
    });
}

window.onload = function() 
{
    init();
};
