
function init()
{
    var url = location.href;
    var key = url.split('?')[1];
    var path = "com_list/" + key;
    var ref = firebase.database().ref(path);
    var content = document.getElementById('content');
    var title = document.getElementById('title');
    notification();
    firebase.auth().onAuthStateChanged(function(user){
        if(user)
        {
            ref.once('value').then(function(snapshot){
                if(user.uid != snapshot.val().writerUid)
                    window.location.href = "index.html?1"; 
            });
        }
        else 
        {
            window.location.href = "index.html?1";
        }
    });
    ref.once('value').then(function(snapshot){
        title.value = snapshot.val().title;
        content.value = snapshot.val().content;
    }).then(function(){
        document.getElementById('loading').style.display = "none";
    });

    document.getElementById('btnSubmit').addEventListener('click', function(){
        if(title.value != "" && content.value != "")
        {
            var nowTime = new Date();
            var year = nowTime.getFullYear();
            var month = (nowTime.getMonth()+1 < 10 ? '0' : '') + (nowTime.getMonth()+1);
            var date = (nowTime.getDate() < 10 ? '0' : '') + (nowTime.getDate());
            var hour = (nowTime.getHours() < 10 ? '0' : '') + (nowTime.getHours());
            var minute = (nowTime.getMinutes() < 10 ? '0' : '') + (nowTime.getMinutes());
            var second = (nowTime.getSeconds() < 10 ? '0' : '') + (nowTime.getSeconds());
            var editTime = year + "-" + month + "-" + date + " " + hour + ":" + minute;
            firebase.database().ref(path).update({
                title: title.value,
                content: content.value,
                postTime: editTime,
                updateTime: editTime,
                sortTime: parseInt('-' + year + month + date + hour + minute + second)
            }).then(function(){
                window.location.href = "article.html?" + key;
            }).catch(function(e){
                alert(e.message);
            });
        }
        else if(title.value == "")
            alert("Please enter your title!");
        else
            alert("Please enter your content!")
    }, false);

    document.getElementById('btnCancel').addEventListener('click', function(){
        window.location.href = "article.html?" + key;
    });
}

window.onload = function () {
    init();
};