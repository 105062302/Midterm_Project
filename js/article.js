
function init()
{
    var url = location.href;
    var key = url.split('?')[1];
    var path = "com_list/" + key;
    var commentPath = path + "/comment";
    var ref = firebase.database().ref(path);
    var btnComment = document.getElementById('btnComment');
    var editArticle = document.getElementById('editArticle');
    var btnEdit = document.getElementById('edit');
    var btnDelete = document.getElementById('delete');
    var comment = document.getElementById('comment');
    notification();
    firebase.auth().onAuthStateChanged(function(user){
        if(user)
        {
            if(!user.emailVerified)
            {
                comment.style.display = "none";
                btnComment.style.display = "none";
            }
            ref.once('value').then(function(snapshot){
                if(!snapshot.exists())
                {
                    window.location.href = "index.html?1";

                }
                else
                {
                    var writerUid = snapshot.val().writerUid;
                    if(writerUid != user.uid)
                    {
                        editArticle.style.display = "none";
                    }
                }
            });
        }
        else
        {
            window.location.href = "index.html?1";
        }
    });

    ref.once('value').then(function(snapshot){
        document.getElementById('articleTitle').innerText = snapshot.val().title;
        var writerRef = firebase.database().ref('users/' + snapshot.val().writerUid);
        writerRef.once('value').then(function(childSnapshot){
            var data = childSnapshot.val();
            document.getElementById('articleWriter').innerHTML = "<img class = 'rounded' src = " + data.photoURL + " slt = '' width = '30' height = '30'> " + snapshot.val().writer + "<span style = 'font-size: 10px; color: rgba(0,0,0,0.4);'> " + snapshot.val().postTime + "</span>";
            document.getElementById('articleContent').innerText = snapshot.val().content;
        })
    });

    btnEdit.addEventListener('click', function(){
        window.location.href = "edit.html?" + key;
    }, false);  

    btnDelete.addEventListener('click', function(){
        if (window.confirm('Are you sure you want to delete this post ?') == true)
        {
            firebase.database().ref(path).remove().then(function(){
                window.location.href = "index.html?1";
            });
        }
    });

    btnComment.addEventListener('click', function(){
        var currentUser = firebase.auth().currentUser;
        if (comment.value != ""){
            var ref = firebase.database().ref(commentPath);
            var nowTime = new Date();
            var year = nowTime.getFullYear();
            var month = (nowTime.getMonth()+1 < 10 ? '0' : '') + (nowTime.getMonth()+1);
            var date = (nowTime.getDate() < 10 ? '0' : '') + (nowTime.getDate());
            var hour = (nowTime.getHours() < 10 ? '0' : '') + (nowTime.getHours());
            var minute = (nowTime.getMinutes() < 10 ? '0' : '') + (nowTime.getMinutes());
            var second = (nowTime.getSeconds() < 10 ? '0' : '') + (nowTime.getSeconds());
            var commentTime = year + "-" + month + "-" + date + " " + hour + ":" + minute;
            var data = {
                comment: comment.value,
                writer: currentUser.displayName,
                writerUid: currentUser.uid,
                commentTime: commentTime
            };
            ref.push(data);
            comment.value = "";
            firebase.database().ref(path).update({
                updateTime: commentTime,
                sortTime: parseInt('-' + year + month + date + hour + minute + second)
            });
            var commentRef = firebase.database().ref('users/' + currentUser.uid);
            commentRef.once('value').then(function(snapshot){
                var data = snapshot.val();
                var commentTimes = parseInt(data.commentTimes) + 1;
                firebase.database().ref('users/' + currentUser.uid).update({
                    commentTimes: commentTimes
                });
            });
        }
    });

    var str_before_writer = "<div class = 'my-1 p-3 rounded box-shadow' style = 'background-color:rgba(0,0,0,0.1); word-break: break-all; word-wrap:break-word;'><h6 class = 'border-bottom border-secondary'>";
    var str_before_comment = "</span></h6><pre>";
    var str_after_comment = "</pre></div>\n";

    var commentsRef = firebase.database().ref(commentPath);
    var usersRef = firebase.database().ref('users');
    var photoURL;
    var idx = 0;
    var total_comment = [];
    var textConvert = [];
    var first_count = 0;
    var second_count = 0;
    var tmp = 0;

    commentsRef.once('value').then(function (snapshot) {
        usersRef.once('value').then(function(userSnapshot){
            snapshot.forEach(function (childSnapshot){
                var childData = childSnapshot.val();
                var commentWriter = childData.writer;
                var writerUid = childData.writerUid;
                var comment = childData.comment;
                var commentTime = childData.commentTime;
                var tmp2 = tmp.toString();
                userSnapshot.forEach(function(userChildSnapshot){
                    if(userChildSnapshot.key == writerUid)
                    { 
                        photoURL = userChildSnapshot.val().photoURL;
                        total_comment[total_comment.length] = str_before_writer + "<img class = 'rounded' src = " + photoURL + " alt = '' width = '30' height = '30'> " + commentWriter + "<span style = 'float: right;'>" + commentTime + str_before_comment + "<span id = " + tmp2 + "></span>" + str_after_comment;
                        first_count += 1;
                        textConvert[textConvert.length] = comment;
                        tmp += 1;
                    }
                }); 
            });
            document.getElementById('comment_list').innerHTML = total_comment.join('');
            for(i = 0; i < tmp; i++)
            {
                var tmp2 = i.toString();
                document.getElementById(tmp2).innerText = textConvert[i];
            }
            commentsRef.on('child_added', function(data){
                second_count += 1;
                if(second_count > first_count)
                {
                    var childData = data.val();
                    var commentWriter = childData.writer;
                    var writerUid = childData.writerUid;
                    var comment = childData.comment;
                    var commentTime = childData.commentTime;
                    var tmp2 = tmp.toString();
                    userSnapshot.forEach(function(userChildSnapshot){
                        if(userChildSnapshot.key == writerUid)
                        {
                            photoURL = userChildSnapshot.val().photoURL;
                            total_comment[total_comment.length] = str_before_writer + "<img class = 'rounded' src = " + photoURL + " alt = '' width = '30' height = '30'> " + commentWriter + "<span style = 'float: right;'>" + commentTime + str_before_comment + "<span id = " + tmp2 + "></span>" + str_after_comment;
                            textConvert[textConvert.length] = comment;
                            tmp += 1;
                        }
                    });
                    document.getElementById('comment_list').innerHTML = total_comment.join('');
                    for(i = 0; i < tmp; i++)
                    {
                        var tmp2 = i.toString();
                        document.getElementById(tmp2).innerText = textConvert[i];
                    }
                }
            });
        }); 
    }).then(function(){
        document.getElementById("loading").style.display = "none";
    }).catch(e => console.log(e.message));
}

window.onload = function () 
{
    init();
};

