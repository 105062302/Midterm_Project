function init() 
{
    notification();
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('menu');
        if (user) 
        {
            var email = user.email;
            var displayName = user.displayName;
            var photoURL = user.photoURL;
            var profileURL = "profile.html?" + user.uid;
            menu.innerHTML = "<li class='nav-item dropdown'>" +
            "<a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" +
            "<img class = 'rounded' src = " + photoURL + " alt = '' height = '30' width = '30'> " + displayName + "</a><div class='dropdown-menu' aria-labelledby='navbarDropdown'><a class='dropdown-item' href=" + profileURL + ">Profile</a><div class='dropdown-divider'></div>" +
            "<a class='dropdown-item' href=" + "historyPost.html?" + user.uid + ">History Post</a></div></li><li class='nav-item'><a class='nav-link' href='index.html?1'>Home</a></li>";
        } 
        else 
        {
            menu.innerHTML = "";
            document.getElementById('post_list').innerHTML = "";
            document.getElementById('loading').style.display = "none";
        }
    });

    var str_before_title = "<div class = 'my-1 p-3 rounded box-shadow text-muted media'>";
    var str_after_title = "</div>\n";

    var postsRef = firebase.database().ref('com_list').orderByChild('sortTime');
    var total_post = [];
    var tmp = 0;
    var tmpTitle = [];

    postsRef.once('value').then(function(snapshot) {
        snapshot.forEach(function (childSnapshot){
            var childData = childSnapshot.val();
            var articleTitle = childData.title;
            var articleUpdateTime = childData.updateTime;
            var dataLink = "article.html?" + childSnapshot.key;
            if(childData.writerUid == firebase.auth().currentUser.uid)
            {
                var tmp2 = tmp.toString();
                total_post[total_post.length] = str_before_title + "<a class = 'media-body' href = " + dataLink + "><span id = " + tmp2 + "></span>" + "</a><span>" + articleUpdateTime + "</span>" + str_after_title;
                tmpTitle[tmpTitle.length] = articleTitle;
                tmp += 1;
            }
        });
        document.getElementById('post_list').innerHTML = total_post.join('');
        for(i = 0; i < tmp; i++)
        {
            var tmp2 = i.toString();
            document.getElementById(tmp2).innerText = tmpTitle[i];
        }
    }).then(function(){
        document.getElementById('loading').style.display = "none";
    }).catch(e => console.log(e.message));
}

window.onload = function(){
    init();
};
