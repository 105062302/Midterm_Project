# Software Studio 2018 Spring Midterm Project

## Topic
*  Project 主題：Forum: Small Forum
*  Key functions (add/delete)
    1. User Page
    2. Post Page
    3. Post List Page
    4. Leave Comment Under Any Page
*  Other functions (add/delete)
    1. Edit Post
    2. Delete Post
    3. History Post
    4. 文章依據人氣排序
    5. 文章換頁

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description

### Basic Components

* Log In/Sign Up

在進到我的網頁後，首先如果是已註冊的用戶，可以選擇右上角的 ```Log in``` 按鈕進入到登入頁面；而如果是尚未註冊過的使用者，則可以選擇右上角的 ```Sing Up``` 按鈕進入到註冊頁面，而未註冊卻按進登入頁面的使用者，也可以藉由登入頁面中的 ```Not have an account?``` 選項來跳轉進入註冊頁面。在進入註冊頁面後，使用者可以選擇三種註冊方式，分別是網站直接註冊、使用 google 帳號註冊以及 Facebook 帳號註冊。而在使用網站直接註冊的方式時，需要填入使用者的 user name ，這將作為使用者以後使用網站的名字，且不能更改。註冊時需要使用 Email 註冊的方式，所以不能使用直接註冊的方式或 FB 註冊後，又使用同一個 google 帳戶利用 google 註冊的方式註冊，這樣會使得直接註冊的資料被 google 註冊的資料覆蓋。

* Post List Page

在成功登入/註冊網站後，使用者便會以用戶的身份進入主頁，主頁會顯示出在此網頁發文的用戶們所發表的文章列表，此時用戶便可以點進去每一篇文章觀看每一篇文章的內容以及其留言。 如果使用者是以 google 註冊以外的方式註冊的話，帳戶一開始會是未認證的狀態，此時用戶只能觀看每一篇貼文，需要至 ```Profile``` 中點選 ```Email Verify```並至註冊的 Email 信箱收取認證信，才能開啟發文以及留言的功能。

* User Page

在登入網站後，網站右上方會顯示用戶的 ```username``` 以及 ```Log Out``` 的按鈕，而按下用戶名旁邊的倒三角圖示按鈕後，裡面會有 ```Profile``` 和 ```History Post``` 兩個選項可以選，選擇 ```Profile``` 後，就可以進入到 User Page 的頁面。
User Page 中紀錄了用戶的用戶名及UID、Email、Email 認證狀態、帳戶創建時間、上次登入時間以及發文與留言的次數。而在此頁面，用戶可以上傳新的個人頭像來更新網站上的用戶照片。


* Post Page 

在認證完自己的 Email 後，主頁的右下角會出現 ```New Post``` 的藍色按鈕，此時用戶可以開始發表自己的文章。在按下按鈕後，會進入到發文的頁面，用戶即可在輸入文章標題與內容後，發表自己的文章。

而在首頁中的文章列表，用戶可以點擊自己有興趣的文章標題進入該篇文章觀看內容。在文章中可以看到文章標題，標題下方可以看到該文的作者及其頭像，作者名字右側為該篇文章發表的時間，下方為該篇文章的內容及留言。

* Leave Comment Under Any Post

已認證 Email 的用戶在觀看文章時，下面的 Comment 區域會多出一個文字輸入的區塊，可以留下自己對這篇文章的回覆。

* Database & Storage read/Write 

當有新使用者註冊或是有用戶發表新文章和留言，皆會對 Database 進行讀寫的動作，而用戶更新其頭像則是對 Storage 進行讀寫的動作。

* RWD

網頁使用 bootstrap 的框架設計而成，使用者除了使用一般電腦進行體驗外，也可以使用行動裝置進行體驗。

* Host on GitLab Page
https://small-forum-d5537.firebaseapp.com
https://105062302.gitlab.io/Midterm_Project

### Advanced components

* Log In/Sign Up with Google Or Other Third-party Accounts

網站註冊除了使用直接註冊/登入之外，使用者也可以使用 ```Google``` 帳號或是 ```Facebook``` 帳號進行註冊/登入。

* Add Chrome Notification

網站利用 Firebase 當作 Server ，當使用者第一次進入此網站時，google 會跳出是否允許通知的視窗，當使用者允許後，每當有其他用戶在其發表的文章下面留言時，如果此時使用者在線上，Chrome 便會對其發出通知，通知的內容為 ```XXX has commented your post !``` 當使用者點擊通知，網頁便會以新分頁的方式導入到那篇文章，也可以選擇關閉通知。

* Use CSS Animation

在使用者點選 ```Profile```、各篇文章及回首頁時，因為網頁需向 Database 取資料，所以在取資料的時間利用 CSS Animation 在 Loading 的時候加入了 Loading 效果，此外，在網頁 Logo 部分也加入了 CSS Animation 來製作左右晃動的動畫。

### Other Functions Not Included In Key Functions

* History Post

進入 ```History Post``` 頁面後，裡頭紀錄者用戶的發文列表，使用戶更方便的管理自己的文章。

* Edit Post

當用戶進入自己發表的文章時，可以選擇再次編輯此篇文章，點擊右上方的 ```Edit Post ``` 後進入編輯頁面，進入頁面後用戶可以選擇 ```Cancel``` 此次編輯或是 Update 文章內容。

* Delete Post

當用戶想要刪除自己的文章時，點擊右上方的 ```Delete Post``` 即可刪除此篇貼文。

* 文章依人氣排序

當一篇文章被建立時，Database 中除了會建立此篇文章的節點外，還會存取此篇文章的創造時間及更新時間，創造時間會根據版主對於此篇文章的編輯做更新，而更新時間則會依據版主對於此篇文章的更新以及其他用戶對於此篇文章的留言時間做更新；此外，首頁的文章列表會根據最後的更新時間做排序而非根據發表時間做排序，所以可以達到留人氣高留言多的文章排在最上面，而越後面則為人氣較低的文章。

* 文章換頁

首頁中一次只會顯示 12 篇文章，超過的部分會存到下一個分頁去，使得使用者不需要一直往下滑尋找文章。

## Security Report (Optional)

當使用者註冊完但是未認證 Email 時，並不能進行發文以及留言的動作，利用這種做法防止惡意使用者利用ㄧ大群無人帳號在板上發出一堆廢文以及留言。而依據用戶的 UID 管理其發表的文章，防止其他用戶對其發表的文章進行惡意的修改以及刪文，且在一進入 Edit、Profile、Article 和 Content 頁面時，就先判斷登入的使用者的認證狀態以及判斷文章作者是否為登入的使用者，以防止未認證的使用者藉由輸入網址達成不需透過發文按鈕卻能進到發文頁面發文或是明明不是文章作者卻能進到文章編輯頁面竄改文章內容。

使用者在發表文章輸入的內容與留言也透過額外的轉換轉成 innerText 的方式去做儲存，
以防止惡意使用者在文章中輸入類似 tag 的形式 Ex: ```<button>Click me !</button>```等等的方式來攻擊網站。